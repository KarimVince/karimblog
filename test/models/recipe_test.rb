require 'test_helper'

class RecipeTest < ActiveSupport::TestCase


 def setup
    @user = users(:michael)
    @recipe = @user.recipe.build(label: "Lorem ipsum", description: "This is a recipe", 
                         element: "Main", ingredient: "test ingredient")
 end

  test "should be valid" do
    assert @recipe.valid?
  end

  test "user id should be present" do
    @recipe.user_id = nil
    assert_not @recipe.valid?
  end
  
  test "label should be present" do
    @recipe.label = "   "
    assert_not @recipe.valid?
  end

  test "label should be at most 140 characters" do
    @recipe.label = "a" * 141
    assert_not @recipe.valid?
  end
  
  test "description should be present" do
    @recipe.description = "   "
    assert_not @recipe.valid?
  end
  
  test "ingredient should be present" do
    @recipe.description = "   "
    assert_not @recipe.valid?  
  end

  test "element should be present" do
    @recipe.element = "   "
    assert_not @recipe.valid? 
  end
  
end
