require 'test_helper'

class RecipesControllerTest < ActionController::TestCase

  def setup
    @recipe = recipes(:gateau)
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "should get new" do
    get :new
    assert_response :success
  end
  
  test "should redirect update when not logged in" do
   patch :update, id: @recipe, recipe: { label: @recipe.label, element: @recipe.element, description: @recipe.description, 
                                                                  ingredient: @recipe.ingredient}
   assert_not flash.empty?
   assert_redirected_to login_url
  end
  
  test  "should redirect edit when logged in as wrong user" do
    log_in_as(@other_user)
    get :edit, id: @recipe
    assert_not  flash.empty?
    assert_redirected_to recipe_url
  end
  
  test "should redirect update when logged in as wrong user" do
   log_in_as(@other_user)
   patch :update, id: @recipe, recipe: { label: @recipe.label, element: @recipe.element, description: @recipe.description, 
                                                                  ingredient: @recipe.ingredient}
    assert_not flash.empty?
    assert_redirected_to recipe_url  
  end
  
  test "should redirect destroy when not logged in" do
      assert_no_difference 'Recipe.count' do
      delete :destroy, id: @recipe
    end
    assert_redirected_to login_url 
  end
  
  test "should redirect destroy when logged in as non-admin" do
    log_in_as(@other_user)
    assert_no_difference 'Recipe.count' do
      delete :destroy, id: @recipe
    end
    assert_redirected_to  recipe_url  
  end
  
end