require 'test_helper'

class AppliPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
		assert_select "title", "Home | Karim Blog"
  end


end
