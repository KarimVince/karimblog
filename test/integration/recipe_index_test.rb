require 'test_helper'

class RecipeIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:michael)
    @non_admin = users(:archer)
    @recipe = recipes(:gateau)
  end

  test "recipe index as admin including add recipe and delete links" do
    log_in_as(@admin)
    get recipes_path
    assert_template 'recipes/index'

    assert_select 'a[href=?]', recipe_path(@recipe.id)
    assert_select 'a[href=?]', new_recipe_path()     
  
  end

  test "index as non-admin including view recipe" do
    log_in_as(@non_admin)
    get recipes_path
    assert_template 'recipes/index'
    
    assert_select 'a[href=?]', recipe_path(@recipe.id)
    assert_select 'a[href=?]', new_recipe_path()     
  end
  
  test "index as non-logged including view recipe" do
    get recipes_path
    assert_template 'recipes/index'
    
    assert_select 'a[href=?]', recipe_path(@recipe.id)
   assert_select 'a[href=?]', new_recipe_path(), false     
  end

end
