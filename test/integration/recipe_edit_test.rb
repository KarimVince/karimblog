require 'test_helper'

class RecipeEditTest < ActionDispatch::IntegrationTest
    include ApplicationHelper

 def setup
    @user = users(:michael)    
    @recipe = recipes(:gateau)

  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_recipe_path(@recipe.id)
    assert_template 'recipes/edit'
    patch recipe_path(@recipe), recipe: { label:  "",
                                    element: "foo@invalid",
                                    ingredient:              "foo",
                                    description: "bar",
                                     user_id: @user.id }
    assert_template 'recipes/edit'
  end
  
  test "successful edit with friendly forwarding" do
    log_in_as(@user)
    get edit_recipe_path(@recipe.id)
    assert_template 'recipes/edit'
    label  = "Gateau au marron"
    element = "Desert"
    ingredient = "du chocolat"
    description = "Une recete"
    patch recipe_path(@recipe), recipe: { label:  label,
                                    element: element,
                                    ingredient:  ingredient,
                                    description: description, 
                                    user_id: @user.id}
    assert_not flash.empty?
    assert_redirected_to @recipe
    @recipe.reload
    assert_equal label,  @recipe.label
    assert_equal ingredient, @recipe.ingredient
    assert_equal element,  @recipe.element
    assert_equal description, @recipe.description
  end
  
end
