require 'test_helper'

class RecipeShowTest < ActionDispatch::IntegrationTest
 def setup
    @admin = users(:michael)
    @non_admin = users(:archer)
    @recipe_admin = recipes(:gateau)
    @recipe_non_admin = recipes(:creme)
  end

  test "recipe show as admin including edit  and delete links" do
    log_in_as(@admin)
    get recipe_path(@recipe_admin.id)

    assert_select 'a[href=?]', edit_recipe_path(@recipe_admin.id)
    assert_select 'a[href=?]', recipe_path(@recipe_admin.id), text: 'delete'     
  
    assert_difference 'Recipe.count', -1 do
      delete recipe_path(@recipe_admin)
    end
    
    get recipe_path(@recipe_non_admin.id)

    assert_select 'a[href=?]', edit_recipe_path(@recipe_non_admin.id)
    assert_select 'a[href=?]', recipe_path(@recipe_non_admin.id), text: 'delete'     
 
     assert_difference 'Recipe.count', -1 do
      delete recipe_path(@recipe_non_admin)
    end 
  
  end

  test "recipe show as non-admin including edit  and delete links" do
    log_in_as(@non_admin)
    get recipe_path(@recipe_admin.id)

    assert_select 'a[href=?]', edit_recipe_path(@recipe_admin.id), false
    assert_select 'a', text: 'delete', count: 0
 
    assert_difference 'Recipe.count', 0 do
      delete recipe_path(@recipe_admin)
    end 
    
    get recipe_path(@recipe_non_admin.id)

    assert_select 'a[href=?]', edit_recipe_path(@recipe_non_admin.id)
    assert_select 'a[href=?]', recipe_path(@recipe_non_admin.id), text: 'delete'        
    
     assert_difference 'Recipe.count', -1 do
      delete recipe_path(@recipe_non_admin)
    end 
    
  end
  
  test "recipe show as non-logged without edit and delete links" do
    get recipe_path(@recipe_admin.id)
  
    assert_select 'a[href=?]', edit_recipe_path(@recipe_admin.id), false
    assert_select 'a', text: 'delete', count: 0
    
    assert_difference 'Recipe.count', 0 do
      delete recipe_path(@recipe_admin)
    end 
   
  end


end
