class Recipe < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(label: :asc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :label, presence: true, length: { maximum: 140 }
  validates :description, presence: true
  validates :element, presence: true
  validates :ingredient, presence: true
  

end
