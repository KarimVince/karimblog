class ApplicationMailer < ActionMailer::Base
  default from: "noreply@berrada.net"
  layout 'mailer'
end
