class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  
  
    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
	
	    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end
    
    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
    
   # confirm the user of a recipe
   def correct_user_recipe
      @recipe = Recipe.find(params[:id])
        unless @recipe.user_id == current_user.id ||  current_user.admin?
          flash[:danger] =  "You can't amend this recipe"  
          redirect_to recipe_url
        end  
   end 
end
