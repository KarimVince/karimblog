class RecipesController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :destroy]
  before_action :correct_user_recipe,   only: [:edit, :update, :destroy]

  def index
    
    if params[:element_url].blank?
      @recipes = Recipe.all
    else
         @recipes = Recipe.where(:element => params[:element_url]).all

    end
  end
  
  def show
    @recipe = Recipe.find(params[:id])
  end
 
  def new
    @recipe = Recipe.new
  end
 
 def create
    @recipe = Recipe.new(recipe_params)   
    @recipe.user_id = session[:user_id] 
    if @recipe.save
      flash[:info] = "Your recipe have been added. Thanks for contributing"
      redirect_to recipes_url
    else
      render 'new'
    end
  end

  def edit
        @recipe = Recipe.find(params[:id])
  end
  
  def update
    @recipe = Recipe.find(params[:id])
    if @recipe.update_attributes(recipe_params)
      flash[:success] = "Recipe updated"
      redirect_to @recipe
    else
      render 'edit'
    end
  end
  
  def destroy
    Recipe.find(params[:id]).destroy
    flash[:success] = "Recipe deleted"
    redirect_to recipes_url  
  end
  
  private 
  
     def recipe_params
      params.require(:recipe).permit(:label, :element, :description,
                                   :picture, :video, :ingredient)
    end
  
    def exist_recipe
      if Recipe.find(params[:id])
      end
   end 
end
