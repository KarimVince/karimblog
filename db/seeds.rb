# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(name:  "Karim Berrada",
             email: "karim@berrada.net",
             password:              "test123",
             password_confirmation: "test123",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)
Time.zone.now
             
User.create!(name:  "Manon Berrada",
             email: "manon@berrada.net",
             password:              "test123",
             password_confirmation: "test123",
             activated: true,
             activated_at: Time.zone.now)