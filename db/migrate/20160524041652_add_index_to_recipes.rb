class AddIndexToRecipes < ActiveRecord::Migration
  def change
    add_index :recipes, :element
  end
end
